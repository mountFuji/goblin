package base;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import util.psi.PsiClassUtil;

import java.io.IOException;

/**
 * Created by lomoye on 2018/1/7.
 * 基于一个class的action
 */
public abstract class AnClassAction extends AnAction {

    protected abstract void errorCallback(Throwable e, ActionContext context);

    protected abstract void doAction(ActionContext context) throws IOException;

    protected abstract void afterAction(ActionContext context);

    @Override
    public void actionPerformed(AnActionEvent event) {
        ActionContext context = ActionContext.valueOf(event, PsiClassUtil.getCurrentPsiClass(event));

        try {
            doAction(context);

            afterAction(context);
        } catch (Throwable e) {
            errorCallback(e, context);
        }

    }
}
