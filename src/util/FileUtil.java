package util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 杭州蓝诗网络科技有限公司 版权所有 © Copyright 2018<br>
 *
 * @Description: <br>
 * @Project: hades <br>
 * @CreateDate: Created in 2018/8/22 12:03 <br>
 * @Author: <a href="yechangjun@quannengzhanggui.com">lomoye</a>
 */

public class FileUtil {

    public static void write(String fileName, String content, String path) throws IOException {
        File f = new File(path);
        if (!f.exists()) {
            f.mkdirs();
        }
        File file = new File(f, fileName);
        if (!file.exists()) {
            createFile(file, content);
        } else {
            file.delete();
            createFile(file, content);
        }
    }

    private static void createFile(File file, String content) throws IOException {
        FileWriter writer = null;
        try {
            file.createNewFile();
            writer = new FileWriter(file, true);
            writer.write(content);

        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }
}
