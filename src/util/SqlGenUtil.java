package util;

import java.util.HashMap;
import java.util.Map;

/**
 * 杭州蓝诗网络科技有限公司 版权所有 © Copyright 2018<br>
 *
 * @Description: <br>
 * @Project: hades <br>
 * @CreateDate: Created in 2018/8/22 11:01 <br>
 * @Author: <a href="yechangjun@quannengzhanggui.com">lomoye</a>
 */

public class SqlGenUtil {

    /**
     * 默认的实体字段类型和数据库字段类型的映射
     */
    private static final Map<String, String> DEFAULT_TYPE_MAP = new HashMap<>();

    static {
        DEFAULT_TYPE_MAP.put("Integer", "int(20)");
        DEFAULT_TYPE_MAP.put("Long", "bigint(20)");
        DEFAULT_TYPE_MAP.put("String", "varchar(64)");
        DEFAULT_TYPE_MAP.put("Date", "datetime");
        DEFAULT_TYPE_MAP.put("LocalDateTime", "datetime");
        DEFAULT_TYPE_MAP.put("LocalDate", "date");
        DEFAULT_TYPE_MAP.put("LocalTime", "time");
        DEFAULT_TYPE_MAP.put("BigDecimal", "decimal(19,2)");
        DEFAULT_TYPE_MAP.put("Byte[]", "mediumblob");
        DEFAULT_TYPE_MAP.put("Integer", "int(20)");
        DEFAULT_TYPE_MAP.put("Enum", "char(32)");
        DEFAULT_TYPE_MAP.put("Boolean", "int(20)");
        DEFAULT_TYPE_MAP.put("Float", "FLOAT");
        DEFAULT_TYPE_MAP.put("Double", "DOUBLE");
    }

    /**
     * @param name 实体字段名
     * @return 数据库类型
     */
    public static String getFiledType(String name) {
        return DEFAULT_TYPE_MAP.get(name);
    }


    public static String genColumnLine(String field, String classFieldType) {
        String defaultDbFieldType = getFiledType(classFieldType);

        return field + " " + defaultDbFieldType;
    }
}
