package util;


import com.intellij.psi.PsiClass;
import util.psi.NameUtils;
import util.psi.PsiClassUtil;

import java.util.List;

/**
 * Created by tommy on 2016/1/13.
 * 对象属性值的复制
 */
public class DomainCopier {

    public static String copy(PsiClass clazz, String newName, String oldName) {
        List<String> orderedNames = getClassNames(clazz);
        StringBuilder buffer = new StringBuilder();

        for (String name : orderedNames) {
            buffer.append(newName).append(".set").append(upperFirstChar(name));
            buffer.append("(").append(oldName).append(".get").append(upperFirstChar(name)).append("());");
            buffer.append("\n    ");
        }
        return buffer.toString();
    }

    public static String set(PsiClass clazz, String newName) {
        List<String> orderedNames = getClassNames(clazz);
        StringBuilder buffer = new StringBuilder();

        for (String name : orderedNames) {
            buffer.append(newName).append(".set").append(upperFirstChar(name));
            buffer.append("();");
            buffer.append("\n    ");
        }
        return buffer.toString();
    }

    public static String fields(PsiClass clazz) {
        List<String> orderedNames = getClassNames(clazz);
        StringBuilder buffer = new StringBuilder();

        for (String name : orderedNames) {
            buffer.append(NameUtils.camelToUnderline(name)).append(", ");
        }
        return buffer.toString();
    }

    private static String upperFirstChar(String str) {
        char ch = str.charAt(0);
        String endStr = str.substring(1);
        return Character.toUpperCase(ch) + endStr;
    }

    private static List<String> getClassNames(PsiClass clazz) {
        return PsiClassUtil.getAllNames(clazz);
    }
}
