package constant;

/**
 * 杭州蓝诗网络科技有限公司 版权所有 © Copyright 2018<br>
 *
 * @Description: <br>
 * @Project: hades <br>
 * @CreateDate: Created in 2018/8/24 17:42 <br>
 * @Author: <a href="yechangjun@quannengzhanggui.com">lomoye</a>
 */

public class CoreConstants {
    /**
     * @Comment:字段注释标识
     */
    public static final String FILED_DOC_TAG = "Comment";

    /**
     * @NotNull:字段注释标识
     */
    public static final String FILED_NOT_NULL_TAG = "NotNull";
}
