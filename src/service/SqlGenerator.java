package service;

import base.ActionContext;

import java.io.IOException;

/**
 * 杭州蓝诗网络科技有限公司 版权所有 © Copyright 2018<br>
 * sql生成器
 * @Description: <br>
 * @Project: goblin <br>
 * @CreateDate: Created in 2018/8/22 09:34 <br>
 * @Author: <a href="yechangjun@quannengzhanggui.com">lomoye</a>
 */
public interface SqlGenerator {
    /**
     * 生成sql
     */
    String makeSql(ActionContext context);

    /**
     * 保存sql到本地文件
     */
    void saveSql(String fileName, String content, String path) throws IOException;
}
