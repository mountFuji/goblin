package action;

import base.ActionContext;
import base.AnClassAction;
import com.intellij.ide.BrowserUtil;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

/**
 * 2020/2/15 18:39
 * yechangjun
 */
public class GoJavaApiAction extends AnClassAction {
    @Override
    protected void errorCallback(Throwable e, ActionContext context) {

    }

    @Override
    protected void doAction(ActionContext context) throws IOException {
        String docSite = "https://docs.oracle.com/javase/8/docs/api/";

        String classQualifiedName = context.getPsiClass().getQualifiedName();

        if (StringUtils.isNotBlank(classQualifiedName)) {
            classQualifiedName = classQualifiedName.replace(".", "/");
            BrowserUtil.browse(docSite + classQualifiedName + ".html");
        }
    }


    @Override
    protected void afterAction(ActionContext context) {

    }
}
