package action;

import base.WriteTextAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.editor.Caret;
import com.intellij.psi.PsiClass;
import com.yourkit.util.Strings;
import util.DomainCopier;
import util.psi.PsiClassUtil;

/**
 * 杭州蓝诗网络科技有限公司 版权所有 © Copyright 2018<br>
 * 实体转换器
 * @Description: <br>
 * @Project: hades <br>
 * @CreateDate: Created in 2018/9/3 16:45 <br>
 * @Author: <a href="yechangjun@quannengzhanggui.com">lomoye</a>
 */

public class EntityConverter extends WriteTextAction {
    /**
     * 插入的数据内容
     *
     * @param event 事件源
     * @param caret     光标
     * @param selection 被选中的文字 可能为空
     */
    @Override
    protected String content(AnActionEvent event, Caret caret, String selection) {
        PsiClass psiClass = PsiClassUtil.getCurrentPsiClass(event);

        return DomainCopier.copy(psiClass, Strings.isNullOrEmpty(selection) ? "sourceEntity" : selection.trim(),
                "targetEntity");
    }
}
