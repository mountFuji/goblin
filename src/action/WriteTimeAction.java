package action;

import base.WriteTextAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.editor.Caret;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 杭州蓝诗网络科技有限公司 版权所有 © Copyright 2018<br>
 * 插入时间内容
 * @CreateDate: Created in 2018/8/23 14:03 <br>
 * @Author: <a href="yechangjun@quannengzhanggui.com">lomoye</a>
 */

public class WriteTimeAction extends WriteTextAction {

    /**
     * 插入的数据内容
     * @param anActionEvent
     * @param caret 当前光标
     */
    @Override
    protected String content(AnActionEvent anActionEvent, Caret caret, String selection) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(new Date());
    }
}
