package action;

import base.WriteTextAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.editor.Caret;

/**
 * 杭州蓝诗网络科技有限公司 版权所有 © Copyright 2018<br>
 * String转成小写
 * @Description: <br>
 * @Project: hades <br>
 * @CreateDate: Created in 2018/8/30 09:34 <br>
 * @Author: <a href="yechangjun@quannengzhanggui.com">lomoye</a>
 */

public class StringLowCase extends WriteTextAction {
    /**
     * 插入的数据内容
     * @param anActionEvent
     * @param caret 光标
     * @param selection 被选中的文字 可能为空
     */
    @Override
    protected String content(AnActionEvent anActionEvent, Caret caret, String selection) {
        if (selection != null) {
            return selection.toLowerCase();
        }

        return null;
    }
}
