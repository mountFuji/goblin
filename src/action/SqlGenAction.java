package action;

import base.ActionContext;
import base.AnClassAction;
import com.intellij.openapi.ui.Messages;
import service.SqlGenerator;
import service.impl.SqlGeneratorImpl;
import util.psi.NameUtils;

import java.io.IOException;


/**
 * 杭州蓝诗网络科技有限公司 版权所有 © Copyright 2018<br>
 *
 * @Description: <br>
 * @Project: goblin <br>
 * @CreateDate: Created in 2018/8/22 09:45 <br>
 * @Author: <a href="yechangjun@quannengzhanggui.com">lomoye</a>
 */
public class SqlGenAction extends AnClassAction {

    @Override
    protected void errorCallback(Throwable e, ActionContext context) {
        e.printStackTrace();
        Messages.showMessageDialog(context.getProject(), "SQL生成失败:" + e.toString(), "错误", Messages.getInformationIcon());
    }

    @Override
    protected void doAction(ActionContext context) throws IOException {
        SqlGenerator sqlGenerator = new SqlGeneratorImpl();

        String sql = sqlGenerator.makeSql(context);

        sqlGenerator.saveSql(NameUtils.camelToUnderline(context.getClassName()), sql, context.getRootPath());
    }

    @Override
    protected void afterAction(ActionContext context) {
        Messages.showMessageDialog(context.getProject(), "SQL生成完成", "成功", Messages.getInformationIcon());
    }
}
